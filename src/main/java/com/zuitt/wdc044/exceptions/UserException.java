package com.zuitt.wdc044.exceptions;


public class UserException extends Exception {
    // Used to contain a message or an error
    public UserException(String message){
        super(message);
    }
}
